> 代码使用方法见[code.md](./code.md)
## 1. 摘要

在微生物领域，经常需要在培养基上培养细菌，细菌在培养基上会进行繁殖，菌落计数也成为微生物相关工作之一。本文使用最新的菌落检测权威数据集，通过将菌落图像转化为密度图作为标签，使用深度学习方法对菌落图像进行密度预测，最终得到图像中的菌落数量。

## 2. 数据准备
> 数据集：[Annotated dataset for deep-learning-based bacterial colony detection](https://figshare.com/articles/dataset/Annotated_dataset_for_deep-learning-based_bacterial_colony_detection/22022540/3)
> 论文：[Annotated dataset for deep-learning-based bacterial colony detection](https://www.nature.com/articles/s41597-023-02404-8?utm_source=xmol&utm_medium=affiliate&utm_content=meta&utm_campaign=DDCN_1_GL01_metadata#Sec5)

从数据集链接下载数据集（Version 3）之后，会得到如下图的数据集：
![dataset](./pics/dataset.png)

由于我使用[C-3-Framework](https://github.com/gjy3035/C-3-Framework)进行模型训练，依照作者的说法，使用密度检测作为计数方法，而非线性回归出一个计数值。因此，我们需要选择带锚框的标注数据。这里，我选择annot_YOLO.zip里面的标注文件进行处理。

由于C-3-Framework内置了多种数据集的加载方式，因此我们仿照其中的某个数据集进行处理，即可直接使用C-3-Framework的数据集加载接口。这里，我选择仿照QNRF数据集的格式进行数据处理。

首先，创建目录`C-3-Framework-python3.x/datasets/bacterial/img/`，将上面得到的数据集解压至此，再创建三个子目录，分别为`anno、test、train`，将`annot_YOLO.zip`解压至`anno`目录，`test、train`目录用于稍后划分训练集和测试集。

创建文件`C-3-Framework-python3.x/datasets/bacterial/process_data.py`，内容见[链接]()。该脚本对数据的处理为：
- 首先划分训练集和测试集，比例大致为9：1。
- 对图像进行reshape至384x384的大小。
- 创建一个与reshape后图像宽高相同、通道一维的零张量，通过标注数据，将每个菌落的中心点对应的坐标置一，称为密度图。
- 将密度图进行高斯卷积，得到每个位置的菌落密度。
- 将密度图加入输出的`den/`目录，图像加入输出的`img/`目录。

至此，数据集准备完成。

## 3. 模型选择

### 1) VGG
VGG作为传统的卷积神经网络模型，在图像分割领域经常作为backbone使用。我使用C-3-Framework内置的VGG前10层加上上采样作为训练的模型。


### 2) CSRNet
CSRNet是曾经在人群密度检测领域SOTA级别的网络，其为VGG前10层作为backbone，再加上空洞卷积以及上采样，最终得到的网络模型。

### 3) Tail-MLP
在网络进行上采样之前，对1通道的张量做展平、线性变换和ReLU的层。

### 4) VGG with Tail-MLP
在VGG的上采样之前，将张量进行Flatten，然后经过两个不改变大小的线性层，最后重塑回原来的形状。经此再进行上采样操作。
![MLP_VGG](./pics/MLP_VGG.jpg)

### 3) CSRNet with Tail-MLP
与VGG类似。


## 4. 模型训练
模型使用C-3-Framework框架进行训练。

训练结果：

| NetWork | MAE | MSE | Weight |
| -- | -- | -- | -- |
| VGG | 21.79 | 1814.11 | [pretrained](https://pan.baidu.com/s/1jnsCG08ueewI-cKZoBULAQ?pwd=ware) |
| VGG with Tail-MLP | 19.06 | 1954.26 | [pretrained](https://pan.baidu.com/s/1dyEy8Pjuzkx3PoIOcMR0Kw?pwd=ware) |
| CSRNet | 17.60 | 1488.75 | [pretrained](https://pan.baidu.com/s/1UHNxMqBNnl2ptazpLaEoHg?pwd=ware) |
| CSRNet with Tail-MLP | **14.65** | **1307.99** | [pretrained](https://pan.baidu.com/s/17aHDHYit6tGtr5YTDStNKg?pwd=ware) |

训练CSRNet with Tail-MLP模型时，直接使用训练好的CSRNet模型，将CSRNet模型冻结，只保留最后的一个卷积层以及线性层进行训练。
训练结果表明，CSRNet with Tail-MLP相较CSRNet网络，具有更低的MAE和MSE。